import java.sql.*;

public class Main {
	
public static void main(String[] args) {
		
		Connection connection = null;		
		
		try {
			connection = initializeConnection();			
			createTable(connection);
			insertRecords(connection);
			readRecords(connection);			
			closeConnection(connection);

			System.out.println("Done!");
		} 
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}
	
	private static Connection initializeConnection() throws ClassNotFoundException, SQLException {
		Connection c;
		//Class.forName("org.sqlite.JDBC"); //not required any more
		c = DriverManager.getConnection("jdbc:sqlite:test.db");
		System.out.println("Opened database successfully");
		return c;
	}
	
	private static void createTable(Connection c) throws SQLException {
		Statement stmt;
		stmt = c.createStatement();
		String sql = "CREATE TABLE COMPANY " + "(ID INT PRIMARY KEY     NOT NULL,"
				+ " NAME           TEXT    NOT NULL, " + " AGE            INT     NOT NULL, "
				+ " ADDRESS        CHAR(50), " + " SALARY         REAL)";
		stmt.executeUpdate(sql);

		System.out.println("Table created successfully");
	}

	private static void insertRecords(Connection c) throws SQLException {
		Statement stmt;
		String sql;
		stmt = c.createStatement();
		sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " + "VALUES (1, 'Paul', 32, 'California', 20000.00 );";
		stmt.executeUpdate(sql);

		sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " + "VALUES (2, 'Allen', 25, 'Texas', 15000.00 );";
		stmt.executeUpdate(sql);
		System.out.println("Records created successfully");
	}

	private static void readRecords(Connection c) throws SQLException {
		Statement stmt;
		stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM COMPANY;");
		
		System.out.println("Reading records...");
		while (rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("name");
			int age = rs.getInt("age");
			String address = rs.getString("address");
			float salary = rs.getFloat("salary");

			System.out.println("ID = " + id);
			System.out.println("NAME = " + name);
			System.out.println("AGE = " + age);
			System.out.println("ADDRESS = " + address);
			System.out.println("SALARY = " + salary);
			System.out.println();
		}

		rs.close();		
	}
			
	private static void closeConnection(Connection connection) throws SQLException {		
		if (connection!=null)connection.close();
	}

}
